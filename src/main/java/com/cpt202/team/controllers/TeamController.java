package com.cpt202.team.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.ArrayList;
import com.cpt202.team.models.Team;
import com.cpt202.team.repositories.TeamRepo;
import com.cpt202.team.services.TeamService;

@RestController
@RequestMapping("/team")
public class TeamController {
private List<Team> teams = new ArrayList<Team>();

@Autowired
private TeamService teamService;

    @GetMapping("/List")
    public List<Team> getList(){

        return teamService.getTeamList();
    }


    @PostMapping("/add")
    public Team addTeam(@RequestBody Team team){

        return teamService.newTeam(team);
        // teams.add(team);
    }



}
